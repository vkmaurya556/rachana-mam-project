<?php
include_once "database.php";

if(isset($_POST['emailId']))
{
    extract($_POST);
    $password=password_hash($password,PASSWORD_DEFAULT);
    $sql="INSERT INTO user (firstName,lastName,email,password) VALUES ('$firstName','$lastName','$emailId','$password')";
    if(mysqli_query($con,$sql))
    {
        set_session("emailId",$emailId);
        set_session("firstName",$firstName);
        set_session("lastName",$lastName);
        redirect("dashboard.php");
    }
    else
    {
        mysqli_error($con);
    }

}
if(!isset_flash("emailId"))
{
    redirect("index.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signup</title>
</head>
<body>
<h1 align=center>Signup</h1>
<form action="#" method="post">
    <center>
    <table>
        <tr>
            <td>First Name</td>
            <td><input type="text" name='firstName'></td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td><input type="text" name='lastName'></td>
        </tr>
        <tr>
            <td>Email Address</td>
            <td><input type="email" name='emailId' value="<?=get_flash("emailId");?>" readonly></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name='password'></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Sign Up"></td>
        </tr>
    </table>
    </center>
</form>
</body>
</html>
