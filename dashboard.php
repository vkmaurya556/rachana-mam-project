<?php
include_once "database.php";
$msg="";
if(isset($_POST['Update']))
{
    extract($_POST);
    $profilePic="";
    $photo=$_FILES['profile_pic']['tmp_name'];
    if($photo!="")
    {
        $target="photos/$firstName".rand(10,1000).".jpg";
        move_uploaded_file($photo,$target);
        $profilePic="profilePic='$target',";
    }
    
    $sql="
        update 
            user 
        set 
            firstName='$firstName',
            lastName='$lastName',
            $profilePic
            dep='$dep',
            subdep='$sub_dep'
        where 
            email='$emailId'
        ";
        if(mysqli_query($con,$sql))
            $msg= "<font color=green>Record Updated</font>";
        else
            $msg=mysqli_error($con);
}
$data=mysqli_query($con,"Select * from user where email='".get_session("emailId")."'");
$data=$data->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <script src="Assets/jquery.js"></script>
</head>
<body>
    <h1 align="center">Dashboard</h1>
    <!-- <p align="center">
        <b>Email Id :- </b><?=get_session("emailId");?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <b>Name :-</b> <?=get_session("firstName");?> <?=get_session("lastName");?>
    </p> -->
    <p align=center><?=$msg;?> </p>
       
    <form action="#" method="post" enctype="multipart/form-data">
        <center>
        <table>
            <tr>
                <td colspan="3" align=right><a href='Logout.php'>Logout</a></td>
            </tr>
            <tr>
                <td>First Name</td>
                <td><input type="text" name='firstName' value='<?=$data['firstName'];?>'></td>
                <td rowspan=6><img src="<?=$data['profilePic'];?>" width=100 alt=""></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><input type="text" name='lastName' value='<?=$data['lastName'];?>'></td>
            </tr>
            <tr>
                <td>Email Address</td>
                <td><input type="email" name='emailId' value='<?=$data['email'];?>' readonly></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name='password' value='<?=$data['password'];?>'></td>
            </tr>
            <tr>
                <td>Profile Pic</td>
                <td><input type="file" name='profile_pic'></td>
            </tr>     
            <tr>
                <td>Department</td>
                <td>
                    <select name="dep">
                    <option value="">--Choose--</option>
                    <?=getDep("0",$data['dep']);?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Sub department</td>
                <td>
                    <select name="sub_dep">
                    <?=getDep($data['dep'],$data['subdep']);?>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name='Update' value="Update"></td>
            </tr>
        </table>
        </center>
    </form>
</body>
<script>
$("[name=dep]").change(function(){
    $.post("Action.php","cmd=getSubDep&dep="+$(this).val(),function(data){
        console.log(data);
        $("[name=sub_dep]").html(data);
    });
});
</script>
</html>